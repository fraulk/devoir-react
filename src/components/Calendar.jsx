import React, { useState } from 'react'
import moment from 'moment'

export const Calendar = (props) => {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    const today = moment()
    const [currentMonth, setCurrentMonth] = useState(today.month())
    const [currentYear, setCurrentYear] = useState(today.year())
    const date = moment().month(currentMonth).year(currentYear)
    const firstDay = moment(date).startOf('month').format("d")

    const classes = {
        flex: {
            display: 'flex', gap: '1rem'
        }
    }

    const Month = () => {
        const prevMonth = () => currentMonth > 0 ? setCurrentMonth(currentMonth - 1) : setCurrentMonth(11)
        const nextMonth = () => currentMonth < 11 ? setCurrentMonth(currentMonth + 1) : setCurrentMonth(0)

        return (
            <div style={classes.flex}>
                <span onClick={prevMonth}>&#60;&#60;</span>
                <span>{months[currentMonth]}</span>
                <span onClick={nextMonth}>&#62;&#62;</span>
            </div>
        )
    }

    const Year = () => (
        <div style={classes.flex}>
            <span onClick={() => setCurrentYear(currentYear - 1)}>&#60;&#60;</span>
            <span>{currentYear}</span>
            <span onClick={() => setCurrentYear(currentYear + 1)}>&#62;&#62;</span>
        </div>
    )

    const Days = () => {
        const blanks = []
        const daysInMonth = []

        for (let i = 0; i < firstDay; i++) {
            blanks.push(<td key={"blanks-" + i} style={{width: 40}}> </td>)
        }

        for (let d = 1; d <= date.daysInMonth(); d++) {
            daysInMonth.push(<td key={d} style={{width: 40, backgroundColor: (today.day() == d && today.month() == currentMonth && today.year() == currentYear) ? 'crimson' : ''}}>{d}</td>);
        }

        const totalDays = [...blanks, ...daysInMonth]
        const rows = []
        let cells = []

        totalDays.map((day, i) => {
            if(i % 7 != 0) cells.push(day)
            else {
                rows.push(cells)
                cells = []
                cells.push(day)
            }
            if (i == totalDays.length - 1) rows.push(cells)
        })

        return (
            <>
                {rows.map((dayRow, i) => <tr key={"row-" + i}>{dayRow}</tr>)}
            </>
        )
    }

    return (
        <>
            <Month />
            <Year />
            <table>
                <thead>
                    <tr>
                        <td>Di</td>
                        <td>Lu</td>
                        <td>Ma</td>
                        <td>Me</td>
                        <td>Je</td>
                        <td>Ve</td>
                        <td>Sa</td>
                    </tr>
                </thead>
                <tbody><Days /></tbody>
            </table>
        </>
    )
}